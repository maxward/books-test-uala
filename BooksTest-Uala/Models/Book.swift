//
//  Book.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import Foundation

struct BooksResponse: Decodable {
    let books: [Book]
}

struct Book: Decodable {
    
    let id: Int
    let nombre: String
    let autor: String
    let disponibilidad: Bool
    let popularidad: Int
    let imagen: String
    
}
