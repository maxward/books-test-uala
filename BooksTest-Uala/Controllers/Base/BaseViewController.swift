//
//  BaseViewController.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var bgColor: UIColor = .white
    
    override func loadView() {
        
        super.loadView()
        let backgroundView = UIView()
        backgroundView.backgroundColor = bgColor
        self.view = backgroundView
        
    }
}
