//
//  BookDetailsViewController.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

class BookDetailsViewController: UIViewController {
    
    var baseView: BookDetailsView {
        return view as! BookDetailsView
    }
    
    var book: Book!
    
    override func loadView() {
        super.loadView()
        
        view = BookDetailsView(frame: .zero)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseView.configure(with: book)
    }
    
}
