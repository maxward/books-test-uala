//
//  BookListViewController.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

class BookListViewController: UIViewController {
    
    var viewModel: BookListViewModel!
    
    var baseView: BookListView {
        return view as! BookListView
    }
    
    override func loadView() {
        super.loadView()
        
        view = BookListView(frame: .zero)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Biblioteca Uala"
        
        baseView.collectionView.register(BookCollectionViewCell.self, forCellWithReuseIdentifier: "simpleCell")
        baseView.collectionView.delegate = self
        baseView.collectionView.dataSource = self
        
        rightNavButton()
        
        viewModel.fetchBooks(url: URL(string: K.API.books)!, completion: { (status) in
            if status {
                DispatchQueue.main.async {
                    self.baseView.collectionView.reloadData()
                }
            } else {
                // show an alert
            }
        })
    }
    
    fileprivate func rightNavButton() {
        let myButton = UIBarButtonItem(title: "Reorder", style: .plain, target: self,action: #selector(didTapMyButton))
        self.navigationItem.rightBarButtonItem = myButton
    }
    
    @objc fileprivate func didTapMyButton(_ sender: UIBarButtonItem){

        baseView.toggleLayoutSize()
        baseView.collectionView.reloadData()
        baseView.collectionView.collectionViewLayout.invalidateLayout()

    }
}

extension BookListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.books.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simpleCell", for: indexPath) as! BookCollectionViewCell
        cell.configure(with: viewModel.books[indexPath.row])
        return cell
    }
}

extension BookListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let book = viewModel.books[indexPath.row]
        if book.disponibilidad {
            let bookDetailsViewController = BookDetailsViewController()
            bookDetailsViewController.book = book
            navigationController?.pushViewController(bookDetailsViewController, animated: true)
        } else {
            let alert = UIAlertController(title: "Error", message: "El libro no se encuentra disponible", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }
}
