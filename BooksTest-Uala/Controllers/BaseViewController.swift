//
//  BaseViewController.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import Foundation

import UIKit

class CodeableViewController: UIViewController {
    
    var bgColor: UIColor {
        get {
            return UIColor.white
        }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
    }
    
    override func loadView() {
        super.loadView()
        self.view = UIView()
    }
    
}

class SimpleViewController: CodeableViewController {
    
    
    func backgroundView() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.red
        return view
    }
    
    func backgroundView(withColor color: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
    
    override func loadView() {
        super.loadView()
        print(#function)
        view = self.backgroundView(withColor: bgColor)
    }
}
