//
//  BookListView.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

class BookListView: UIView {
    
    var collectionView: UICollectionView!
    var layout: UICollectionViewFlowLayout!
    
    lazy var widthSize = fullWidth()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        addSubview(collectionView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        
        layout.itemSize = widthSize
    }
    
    func toggleLayoutSize() {
        if widthSize == fullWidth() {
            widthSize = quarterPart()
        } else {
            widthSize = fullWidth()
        }
        
        layout.itemSize = widthSize

    }
    
}


