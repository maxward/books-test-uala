//
//  BookCell.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit
import Kingfisher

class BookCollectionViewCell: UICollectionViewCell {

    lazy var bookImageView: UIImageView = UIImageView(frame: .zero)
    
    lazy var titleLabel: UILabel = UILabel(frame: .zero)
    lazy var authorLabel: UILabel = UILabel(frame: .zero)
    lazy var popularityLabel: UILabel = UILabel(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        locateViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func locateViews() {
        
        self.addSubview(bookImageView)
        bookImageView.translatesAutoresizingMaskIntoConstraints = false
        bookImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        bookImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        bookImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        bookImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        bookImageView.contentMode = .scaleAspectFit
        
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: bookImageView.trailingAnchor, constant: 8) .isActive = true
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        
        self.addSubview(authorLabel)
        authorLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        authorLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        
        self.addSubview(popularityLabel)
        popularityLabel.translatesAutoresizingMaskIntoConstraints = false
        authorLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 8).isActive = true
        authorLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
    }
    
    func configure(with book: Book) {
        titleLabel.text = book.nombre
        authorLabel.text = book.autor
        popularityLabel.text = "\(book.popularidad)"
        let url = URL(string: book.imagen)
        bookImageView.kf.setImage(with: url)
    }
}

