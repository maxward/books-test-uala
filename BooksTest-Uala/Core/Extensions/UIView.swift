//
//  UIView.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

extension UIView {
    
    func fullWidth(offset: CGFloat = 0, margins: CGFloat = 0) -> CGSize {
        return CGSize(width: bounds.width - 10, height: 60)
    }
    
    func halfWidth(offset: CGFloat = 0, margins: CGFloat = 0) -> CGSize {
        let sWidth = (frame.width - offset - (margins * 2)) / 2
        return CGSize(width: sWidth, height: sWidth)
    }
    
    func thirdPart(offset: CGFloat = 0, margins: CGFloat = 0) -> CGSize {
        let sWidth = (frame.width - offset - (margins * 2)) / 3
        return CGSize(width: sWidth, height: sWidth)
    }
    
    func quarterPart(offset: CGFloat = 0, margins: CGFloat = 0) -> CGSize {
        let sWidth = (frame.width - offset - (margins * 3)) / 4
        return CGSize(width: sWidth, height: sWidth)
    }
}
