//
//  BooListViewModel.swift
//  BooksTest-Uala
//
//  Created by Max Ward on 28/10/2019.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

enum OrderType {
    case ASC, DESC
}

class BookListViewModel {
    
    var books: [Book] = []
    
    func fetchBooks(url: URL, completion: @escaping ((Bool) -> Void)) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let jsonData = data {
                do {
                    let res = try JSONDecoder().decode([Book].self, from: jsonData)
                    self.books = self.order(bookList: res)
                    completion(true)
                } catch (let err) {
                    print("error decoding object: \(err.localizedDescription)")
                    completion(false)
                }
            }
        }.resume()
    }
    
    func order(bookList: [Book], order: OrderType = .DESC) -> [Book] {
        switch order {
        case .ASC:
            return bookList.sorted { $0.popularidad < $1.popularidad }
        case .DESC:
            return bookList.sorted { $0.popularidad > $1.popularidad }
        }
    }
}
